require File.join(File.dirname(__FILE__), "test_helper")
require "sqlgen"

include SqlGen

describe SqlGen::Select do
  context "Simple queries" do
    should "generate simple queries with fields" do
       Select[:authors].sql.should == "select * from authors"
       Select[:authors].with(:id).sql.should == "select id from authors"
       Select[:authors].with(:authors__id).sql.should == "select authors.id from authors"
       Select[:authors => :a].with(:a__id).sql.should == "select a.id from authors a"
    end

    should "accept a field name verbatim" do
      Select[:authors].with("count(id)").sql.should == "select count(id) from authors"
    end
  end


  context "Queries with conditions" do 
    should "take a condition verbatim" do
      Select[:authors].where("name = \"Brian\"").sql.should ==  "select * from authors where name = \"Brian\""
    end

    should "bind values into a bind clause" do
       Select[:authors].where(["name = ?", "Brian"]).sql.should.should == "select * from authors where name = \"Brian\""
       Select[:authors].where(["name = ? and id = ?", "Brian", 1]).sql.should == "select * from authors where name = \"Brian\" and id = 1"
    end
  end


  context "Queries with orders and groups" do
    should "have an order, ordering by name" do
      Select[:authors].order(:name).sql.should ==  "select * from authors order by name asc"
    end

    should "have an order, ordering by name and id desc" do
      Select[:authors].order(:name).order(:id, :desc).sql.should == "select * from authors order by name asc, id desc"
    end

    should "have a group over name" do
       Select[:authors].group(:name).sql.should == "select * from authors group by name"
    end
  end

  context "Subselects in where conditions" do
    should "have a subselect in a where condition" do
       Select[:authors].where(["id = ?", Select[:articles].with(:id).where(["author_id = ?", 1])]).sql.should == "select * from authors where id = (select id from articles where author_id = 1)"
    end
  end

  context "Queries with having" do
    should "generate a having portion of the query" do
      Select[:authors].with("count(id)").group(:id).having("count(id) > 2").sql.should ==  "select count(id) from authors group by id having count(id) > 2"
    end
  end

  context "Join expressions" do
    should "do various joins on authors and articles" do
       Select[:authors => :a].inner_join({:articles => :ar}, "a.id = ar.author_id").sql.should == "select * from authors a inner join articles ar on a.id = ar.author_id" 
       Select[:authors => :a].left_outer_join({:articles => :ar}, "a.id = ar.author_id").sql.should == "select * from authors a left outer join articles ar on a.id = ar.author_id"
    end

    should "join authors against a subselect" do

       Select[:authors => :a].inner_join({Select[:articles].with(:id, :title, :author_id).where(["title like ?", "The%"]) => :ar}, "a.id = ar.author_id").sql.should == "select * from authors a inner join (select id, title, author_id from articles where title like \"The%\") ar on a.id = ar.author_id"

    end
  end

end
