require File.join(File.dirname(__FILE__), "test_helper")
require "sqlgen"

include SqlGen::Quoting

describe SqlGen::Quoting do

  context "Correct binds" do
    should "bind one parameter" do
       bind(["id = ?", 1]).should == "id = 1"
    end
    
    should "bind two parameters" do
       bind(["id = ? and name = ?", 1, "Brian"]).should == "id = 1 and name = \"Brian\""
    end
  end

  context "Incorrect binds" do
    should "notice incorrect binds" do
      lambda {
        bind(["id = ?", 1, 2])
      }.should raise_error(QuotingError)

      lambda {
        bind(["id = ? and id = ?", 1])
      }.should raise_error(QuotingError)
    end
  end

  context "Various quoting tests" do
    should "pass all quoting tests" do
       quote("test").should == "\"test\""
      # TODO: This is incorrect
#       "''test''", quote("'test'")
#       "\\", quote("\\")
       quote(1).should == "1"
       quote(1.1).should == "1.1"
       quote(Time.local(2010,1,1,23,59,59)).should == "2010-01-01 23:59:59"
       quote(true).should == "'t'"
       quote(false).should == "'f'"
       quote(nil).should == "null"
    end
  end

end
