module SqlGen

  class Insert

    include FieldHelpers

    # Create a new SQL INSERT expression with a table. Example:
    #
    # <tt>Insert[:authors].values(:name => 'Brian')</tt> => <tt>
    # insert into authors (name) values('Brian')
    #
    def self.[](table)
      Insert.new(table)
    end


    def initialize(table)
      @table = table
    end

    def values(fields)
      @fields = fields
    end

    def sql
      fields_as_array = @fields.map {|val| val }
      field_names     = fields_as_array.map {|f| f[0] }.join(", ")
      field_values    = fields_as_array.map {|f| quote(f[1]) }
      
      "insert #{@table} (#{field_names}) values(#{field_values})" 
    end
  end
  
end
