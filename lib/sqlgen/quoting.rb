module SqlGen
  module Quoting
    
    class QuotingError < Exception; end

    def bind(bind_list)
      return bind_list if bind_list.is_a? String

      bind_expr = bind_list.shift
      raise QuotingError, "Incorrect number of values in bind expression." if bind_expr.count('?') != bind_list.length

      bind_expr.gsub(/\?/) {|b| quote(bind_list.shift) }

    end

    def quote(value)
      case value
      when String
        "\"#{value.gsub(/\\/,'\&\&').gsub(/'/, "''")}\""
      when Float, Fixnum, Bignum
        value.to_s
      when Date, Time
        value.strftime("%Y-%m-%d %H:%M:%S")
      when TrueClass
        "'t'" # These are dbms dependent
      when FalseClass
        "'f'"
      when NilClass
        "null"
      when SelectClause
        "(#{value.sql})"
      end
    end

  end
end
