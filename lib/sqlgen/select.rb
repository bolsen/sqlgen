module SqlGen
  
  # Select starts a query. Since it inherits from SelectClause, a query can be chained from this object.
  class Select < SelectClause

    include FieldHelpers

    # Create a new SQL SELECT expression with a table. Example:
    #
    # <tt>Select[:authors]</tt> => <tt> select * from authors</tt>
    # <tt>Select[:authors => :a]</tt> => <tt>select * from authors a</tt>
    # <tt>Select[Select[:authors] => :a]</tt> => <tt>select * from (select * from authors) a
    def self.[](table)
      Select.new(table)
    end
    
    def initialize(table)
      super(self)
      self.query[:table_expr] = [table_alias(table)]
    end

  end

end
