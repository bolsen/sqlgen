module SqlGen

  # This is the main class for performing selects and contains the public API. All sub-clauses
  # inherit from SelectClause.

  class SelectClause

    attr_accessor :query

    # If this is a new query, do nothing. Otherwise, copy the contents of the original query.
    def initialize(original = nil)
      self.query ||= {}
      unless original.nil?
        self.query = original.query
      end
    end

    # Generate an SQL expression from the current query data.
    def sql
      "select " +
        unless self.query[:with].nil? || self.query[:with].length == 0
          self.query[:with].join(", ")
        else
          "*"
        end +
        " from " + self.query[:table_expr].join(" ") +
        unless self.query[:where].nil? || self.query[:where].length == 0
          " where #{self.query[:where].join(" ")}"
        else
          ""
        end + 
        unless self.query[:order].nil? || self.query[:order].length == 0
          " order by #{self.query[:order].join(", ")}"
        else
          ""
        end +
        unless self.query[:group].nil? || self.query[:group].length == 0
          " group by #{self.query[:group].join(", ")}"
        else
          ""
        end +
        unless self.query[:having].nil? || self.query[:having].length == 0
          " having #{self.query[:having].join(", ")}"
        else
          ""
        end  
    end


    alias_method :to_s, :sql

    # List the fields that should be returned in the reslt set.
    # Can take fields as symbols or strings. 
    # A symbol will be preprocessed. For example:
    # * <tt>:id</tt> will be <tt>id</tt>
    # * <tt>:authors__id</tt> will be <tt>authors.id</tt>
    def with(*fields)
      WithClause.new(fields, self)
    end

    # Specify a where clause. Can take a string or an array which is 
    # used to bind parameters to the condition string. Example:
    #
    # * <tt>.where("id = 1")</tt> will be just <tt>id = 1</tt> in the SQL.
    # * <tt>.where(["id = ?", 1])</tt> will be <tt>id = 1</tt> in the SQL.
    #
    # Look at Quoting#quote for the quoting capabilities.
    def where(conditions)
      WhereClause.new(conditions, self)
    end

    # Adds an ORDER BY to the query. A single field is specified with an
    # optional ordering parameter.
    # Example:
    #
    # <tt>.order(:id, :desc)</tt> will produce <tt>order by id desc</tt> to the SQL.
    #
    # The field is processed similar to return columns in <tt>SelectClause#with</tt>.
    def order(field, by=:asc)
      OrderClause.new(field, by, self)
    end

    # Adds a GROUP BY to the query. Multiple fields can be specified for grouping.
    # Example:
    #
    # <tt>.group(:id, :name)</tt> will add <tt>group by id, name</tt>
    #
    # The field is processed similar to return columns in <tt>SelectClause#with</tt>.

    def group(*fields)
      GroupClause.new(fields, self)
    end

    # Adds a HAVING clause to the query. Multiple fields can be specified. This accepts strings 
    # Using the binding and quoting similar to where queries.
    # Example:
    #
    # <tt>.having(["count(id) > ?", 2], "length(name) > 5")</tt>
    
    def having(*fields)
      HavingClause.new(fields, self)
    end

    # Adds an INNER JOIN to the query. If the table name is provided as a hash, then it is considered
    # as a table alias expression.
    # Example:
    #
    # <tt>.inner_join(:articles)</tt>
    # <tt>.inner_join(:articles, "authors.id = articles.author_id")</tt>
    # <tt>.inner_join({:articles => :a}, "authors.id = a.author_id")</tt>
    #
    # All joins can accept SelectClauses as subselects:
    #
    # <tt>.inner_join({Select[:articles].where(["name = ?", "Brian"]) => :a}, "authors.id = a.author_id")</tt>

    def inner_join(table, on=nil)
      JoinClause.new(table, on, :inner_join, self)
    end

    # Adds an LEFT OUTER JOIN to the query. If the table name is provided as a hash, then it is considered
    # as a table alias expression.
    # Example:
    #
    # <tt>.left_outer_join(:articles)</tt>
    # <tt>.left_outer_join(:articles, "authors.id = articles.author_id")</tt>
    # <tt>.left_outer_join({:articles => :a}, "authors.id = a.author_id")</tt>

    def left_outer_join(table, on=nil)
      JoinClause.new(table, on, :left_outer_join, self)
    end

    # Adds an RIGHT OUTER JOIN to the query. If the table name is provided as a hash, then it is considered
    # as a table alias expression.
    # Example:
    #
    # <tt>.right_outer_join(:articles)</tt>
    # <tt>.right_outer_join(:articles, "authors.id = articles.author_id")</tt>
    # <tt>.right_outer_join({:articles => :a}, "authors.id = a.author_id")</tt>

    def right_outer_join(table, on=nil)
      JoinClause.new(table, on, :right_outer_join, self)
    end

  end

end
