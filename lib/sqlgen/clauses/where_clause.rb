module SqlGen
  # A WhereClause is a SelectClause that processes conditions and adds them into the
  # SQL query.

  class WhereClause < SelectClause
    
    include FieldHelpers
    include Quoting

    def initialize(conditions, original = nil)
      super(original)
      
      self.query[:where] ||= []
      andstr = if self.query[:where].length != 0
                 " and " 
               else
                 ""
               end
      self.query[:where] << (andstr + bind(conditions))
    end
  end

  AndClause = WhereClause

end
