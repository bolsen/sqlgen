module SqlGen
  # A GroupClause is a SelectClause that applies the GROUP BY part of an SQL query.

  class GroupClause < SelectClause
    
    include FieldHelpers

    def initialize(fields, original = nil)
      super(original)
      
      self.query[:group] ||= []
      self.query[:group] += fields.map {|field| qualified_field_path(field) }
    end
  end

end
