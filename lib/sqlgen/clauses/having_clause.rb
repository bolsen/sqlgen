module SqlGen
  # A WhereClause is a SelectClause that processes conditions and adds them into the
  # SQL query.

  class HavingClause < SelectClause
    
    include FieldHelpers
    include Quoting

    def initialize(conditions, original = nil)
      super(original)
      
      self.query[:having] ||= []
      self.query[:having] << bind(conditions)
    end
  end

end
