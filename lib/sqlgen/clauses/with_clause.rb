module SqlGen
  # A WithClause is a SelectClause that applies the fields that should be returned in the 
  # SQL query.

  class WithClause < SelectClause
    
    include FieldHelpers

    def initialize(fields, original = nil)
      super(original)
      
      self.query[:with] ||= []
      self.query[:with] += fields.map {|field| qualified_field_path(field) }
    end
  end

end
