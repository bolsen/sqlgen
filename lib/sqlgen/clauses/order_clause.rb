module SqlGen
  # A WithClause is a SelectClause that applies the fields that should be returned in the 
  # SQL query.

  class OrderClause < SelectClause
    
    include FieldHelpers

    def initialize(field, by=:asc, original = nil)
      super(original)
      
      self.query[:order] ||= []
      self.query[:order] << "#{qualified_field_path(field)} #{by}"
    end
  end

end
