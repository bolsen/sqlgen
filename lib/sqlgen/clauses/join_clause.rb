module SqlGen
  class JoinClause < SelectClause
    include FieldHelpers

    def initialize(table_expr, on="", type=:inner_join, original=nil)
      super(original)
      on_clause = on != "" ? " on #{on}" : ""
      self.query[:table_expr] ||= []
      self.query[:table_expr] << "#{join_type(type)} #{table_alias(table_expr)}#{on_clause}"
      
    end

    private

    def join_type(type)
      case type
        when :inner_join
        "inner join"
        when :left_outer_join
        "left outer join"
        when :right_outer_join
        "right outer join"
      end
    end

  end

end
