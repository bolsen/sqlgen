module SqlGen

  # This class is for handling insert queries.

  class InsertClause

    attr_accessor :query

    def initialize(original = nil)
      self.query ||= {}
      unless original.nil?
        self.query = original.query
      end
    end


    def sql

    end

    def values(fields)
    end
    
  end
  
end
