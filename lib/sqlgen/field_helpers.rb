module SqlGen
  include Quoting
  module FieldHelpers

    # Converts a symbol of :a__b to a.b, or accepts a string verbatim
    def qualified_field_path(field)
      if field.is_a? Symbol
        field.to_s.gsub(/__/, '.')
      else
        field
      end
    end

    # Convert a table alias phrase in a hash to an SQL string.
    def table_alias(table)
      if table.is_a? Symbol
        table.to_s
      elsif table.is_a? Hash
        if table.keys[0].is_a? SelectClause
          "#{quote(table.keys[0])} #{table.values[0]}"
        else
          "#{table.keys[0]} #{table.values[0]}"
        end
      end
    end

  end
end
